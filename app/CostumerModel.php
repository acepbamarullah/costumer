<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostumerModel extends Model
{
    //
    protected $table = 'costumer';
    protected $fillable = ['name','password','email','is_married','gender','address'];
}
