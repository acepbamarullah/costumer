<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V01</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">#</th>
								<th class="column2">Name</th>
								<th class="column3">Email</th>
								<th class="column4">Gender</th>
								<th class="column5">Marital Status</th>
								<th class="column6">Action</th>
							</tr>
						</thead>
						<tbody>
								<tr>
									<td class="column6">1</td>
									<td class="column2">Nanang</td>
									<td class="column3">nanang@gmail.com</td>
									<td class="column4">Male</td>
									<td class="column5">Married</td>
									<td class="column1"><a class="btn btn-info" href="" title="Update"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>&nbsp;<a class="btn btn-success" href="" title="Detail"><i class="fa fa-info-circle" aria-hidden="true"></i></a>&nbsp;<a class="btn btn-danger" href="" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
								</tr>
								<tr>
									<td class="column6">1</td>
									<td class="column2">Nanang</td>
									<td class="column3">nanang@gmail.com</td>
									<td class="column4">Male</td>
									<td class="column5">Married</td>
									<td class="column1"><a class="btn btn-info" href="" title="Update"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>&nbsp;<a class="btn btn-success" href="" title="Detail"><i class="fa fa-info-circle" aria-hidden="true"></i></a>&nbsp;<a class="btn btn-danger" href="" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
								</tr>
								
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>