<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\CostumerModel;
class CostumerController extends Controller
{
    //
    public function index()
    {
        $data['data'] = CostumerModel::all();
        return view('index',$data);

    }

    public function show($id)
    {
        $returnOutput = [];
        try {
            $costumer = CostumerModel::find($id);
            $returnOutput['status']['code'] = $costumer ? 200 : 204;
            $returnOutput['status']['response'] = $costumer ? "success" : "failed";
            $returnOutput['status']['message'] = $costumer ? "Data found" : "Data not found";
            $returnOutput['result'] = $costumer;
          }
          catch(QueryException $e) {
            $returnOutput['status']['code'] = $e->errorInfo[1];
            $returnOutput['status']['response'] = "error";
            $returnOutput['status']['message'] = $e->errorInfo[2];
            $returnOutput['result'] = $request->all();
          }
        return response()->json($returnOutput, 201);
    }

    public function store(Request $request)
    {
        $returnOutput = [];
        try {
            $costumer = CostumerModel::create($request->all());
            $returnOutput['status']['code'] = $costumer ? 201 : 500;
            $returnOutput['status']['response'] = $costumer ? "success" : "failed";
            $returnOutput['status']['message'] = $costumer ? "Data added successfully" : "Data failed to add";
            $returnOutput['result'] = $request->all();
          }
          catch(QueryException $e) {
            $returnOutput['status']['code'] = $e->errorInfo[1];
            $returnOutput['status']['response'] = "error";
            $returnOutput['status']['message'] = $e->errorInfo[2];
            $returnOutput['result'] = $request->all();
          }
        return response()->json($returnOutput, 201);
    }

    public function update(Request $request, $id)
    {
        try {
            $costumer = CostumerModel::find($id);
            $costumer->name = $request->name;
            $costumer->address = $request->address;
            $costumer->email = $request->email;
            $costumer->is_married = $request->is_married;
            $costumer->gender = $request->gender;
            $costumer->save();
            $returnOutput['status']['code'] = $costumer->update() ? 200 : 204;
            $returnOutput['status']['response'] = $costumer->update() ? "success" : "failed";
            $returnOutput['status']['message'] = $costumer->update() ? "Data found" : "Data not found";
            $returnOutput['result'] = $costumer;
          }
          catch(QueryException $e) {
            $returnOutput['status']['code'] = $e->errorInfo[1];
            $returnOutput['status']['response'] = "error";
            $returnOutput['status']['message'] = $e->errorInfo[2];
            $returnOutput['result'] = $request->all();
          }
        return response()->json($returnOutput, 200);
    }

    public function delete($id)
    {
        try {
            $costumer = CostumerModel::find($id);
            $costumer->delete();
            $returnOutput['status']['code'] = 200;
            $returnOutput['status']['response'] = "success";
            $returnOutput['status']['message'] = "Data deleted successfully";
            $returnOutput['result'] = $costumer;
          }
          catch(QueryException $e) {
            $returnOutput['status']['code'] = $e->errorInfo[1];
            $returnOutput['status']['response'] = "error";
            $returnOutput['status']['message'] = $e->errorInfo[2];
            $returnOutput['result'] = $request->all();
          }
        return response()->json($returnOutput, 200);
    }
}
