<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V01</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

</head>
<body>
	<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header table100-head">
        <h5 class="modal-title" id="exampleModalLabel">User detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="showProfile()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  	<table id="showProfile">
		<tr>
			<td>Name</td>
			<td><span id="name">Loading, please wait...</span></td>
		</tr>
		<tr>
			<td>Gender</td>
			<td><span id="gender">Loading, please wait...</span></td>
		</tr>
		<tr>
			<td>Address</td>
			<td><span id="address">Loading, please wait...</span></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><span id="email">Loading, please wait...</span></td>
		</tr>
		<tr>
			<td>Marital status</td>
			<td><span id="is_married">Loading, please wait...</span></td>
		</tr>
		</table>

		<table id="editProfile" style="display:none">
		<tr class="hidden" style="display:none">
			<td>Id</td>
			<td class="form-group"><input type="text" class="form-control" name="id" id="idl"></input></td>
		</tr>
		<tr>
			<td>Name</td>
			<td class="form-group"><input type="text" class="form-control" name="name" id="namel"></input></td>
		</tr>
		<tr>
			<td>Gender</td>
			<td class="form-group">
				<input type="text" class="form-control hidden" name="gender" id="genderl" style="display:none"></input>
				<select class="form-control" id="genderSel" onchange="$('#genderl').val($('#genderSel :selected').val())">
					<option value="m">Male</option>
					<option value="f">Female</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Address</td>
			<td class="form-group"><input type="text" class="form-control" name="address" id="addressl"></input></td>
		</tr>
		<tr>
			<td>Email</td>
			<td class="form-group"><input type="text" class="form-control" name="email" id="emaill"></input></td>
		</tr>
		<tr>
			<td>Marital status</td>
			<td class="form-group">
				<input type="text" class="form-control" name="is_married" id="is_marriedl" style="display:none"></input>
				<select class="form-control" id="isMarriedSel" onchange="$('#is_marriedl').val($('#isMarriedSel :selected').val())">
					<option value="1">Married</option>
					<option value="0">Not Married</option>
				</select>
			</td>
		</tr>
		</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-bawah" data-dismiss="modal" style="display:none" onclick="showProfile()">Close</button>
		<button type="button" class="btn btn-primary btn-bawah" onclick="showEdit()" id="editButton" style="display:none">Edit</button>
		<button type="button" class="btn btn-success" onclick="updateProcess();showProfile();" id="updateButton" style="display:none">Update</button>

      </div>
    </div>
  </div>
</div>

	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
				
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">#</th>
								<th class="column2">Name</th>
								<th class="column3">Email</th>
								<th class="column4">Gender</th>
								<th class="column5">Marital Status</th>
								<th class="column6">Action</th>
							</tr>
						</thead>
						<tbody>
								@foreach($data as $key => $dt)
								<tr id="row{{ $dt->id }}">
									<td class="column6">{{ $key + 1 }}</td>
									<td class="column2" id="name{{ $dt->id }}">{{ $dt->name }}</td>
									<td class="column3" id="email{{ $dt->id }}">{{ $dt->email }}</td>
									<td class="column4" id="gender{{ $dt->id }}">{{ $dt->gender == "m" ? "Male" : "Female" }}</td>
									<td class="column5" id="is_married{{ $dt->id }}">{{ $dt->is_married ? "Merried" : "Not Married"}}</td>
									<td class="column1" id="actionCol{{ $dt->id }}">
										<a class="btn btn-success a1{{ $dt->id }}" href="" onclick="getData({{ $dt->id }})" data-toggle="modal" data-target="#exampleModal" title="Detail and Update"><i class="fa fa-info-circle" aria-hidden="true"></i></a>&nbsp;
										<a class="btn btn-danger a1{{ $dt->id }}" href="#" onclick="$('.a1{{ $dt->id }}').hide();$('.a2{{ $dt->id }}').show();" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
										<a class="btn btn-danger a2{{ $dt->id }}" href="#" onclick="deleteProcess({{ $dt->id }})" title="Process delete" style="display:none">Confirm</a>
										<a class="btn btn-warning a2{{ $dt->id }}" href="#" onclick="$('.a1{{ $dt->id }}').show();$('.a2{{ $dt->id }}').hide();" title="Cancel delete" style="display:none">Cancel</a></td>
								</tr>
								@endforeach
								
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<script>
		function showProfile(){
			$('#updateButton').hide();
			$('#editButton').show();
			$('#editProfile').hide();
			$('#showProfile').show();
			$('#exampleModalLabel').text('User detail');
			var id = $('#idl').val();
			getData(id);
		}
		function showEdit(){
			$('#updateButton').show();
			$('#editButton').hide();
			$('#editProfile').show();
			$('#showProfile').hide();
			$('#exampleModalLabel').text('Edit profile');
		}
		function getData(id){
			axios.get('http://localhost:8000/api/costumer/'+id).then(res=>{
				const entries = Object.entries(res.data.result);
				for (const [kunci, isi] of entries) {
					if(`${kunci}` == 'is_married' && `${isi}` == 1){
						$(`#${kunci}`).text('Married');
						$(`#${kunci}l`).val(`${isi}`);
					}else if(`${kunci}` == 'is_married' && `${isi}` !== 1){
						$(`#${kunci}`).text('Not Married');
						$(`#${kunci}l`).val(`${isi}`);
					}else if(`${kunci}` == 'gender' && `${isi}` == 'm'){
						$(`#${kunci}`).text('Male');
						$(`#${kunci}l`).val(`${isi}`);
					}else if(`${kunci}` == 'gender' && `${isi}` !== 'm'){
						$(`#${kunci}`).text('Female');
						$(`#${kunci}l`).val(`${isi}`);

					}else{
						$(`#${kunci}`).text(`${isi}`);
						$(`#${kunci}l`).val(`${isi}`);
					}
					$('.btn-bawah').show();
				}
			}).catch(err=>{
				console.log(err)
			});
		}
		function updateProcess(){
			var id = parseInt($('#idl').val());
			if(id !== ''){
				axios.put('http://localhost:8000/api/costumer/'+id,{ name: $(`#namel`).val(), address: $(`#addressl`).val(), email: $(`#emaill`).val(), gender: $(`#genderl`).val(),is_married: $(`#is_marriedl`).val() }).then(res=>{
					if(res.data.status.response == 'success'){
						$('#name'+id).text($(`#namel`).val());
						$('#email'+id).text($(`#emaill`).val());
						$('#gender'+id).text($(`#emaill`).val() == 'm' ? 'Male' : 'Female');
						$('#is_married'+id).text($(`#is_marriedl`).val() == '0' ? 'Not Married' : 'Married');
					}
				}).catch(err=>{
					console.log(err)
				});
			}
		}
		function deleteProcess(id){
				axios.delete('http://localhost:8000/api/costumer/'+id).then(res=>{
					if(res.data.status.response == 'success'){
						$('#row'+id).hide();
					}
				}).catch(err=>{
					console.log(err)
				});


		}
	</script>

</body>
</html>
